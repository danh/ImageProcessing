#!/usr/bin/env python
import os
import numpy as np
import cv2  # requires package python-opencv
import argparse

# Adapted from http://knowpapa.com/opencv-rgb-split/
# and https://github.com/mariaokounkova/art_online

def split_into_rgb_channels(image):
  '''Split the target image into its red, green and blue channels.
  image - a numpy array of shape (rows, columns, 3).
  output - three numpy arrays of shape (rows, columns) and dtype same as
           image, containing the corresponding channels.
  '''
  red = image[:,:,2]
  green = image[:,:,1]
  blue = image[:,:,0]
  return red, green, blue

def decompose(filename):
  ''' This function splits an image into rgb channels.
   It saves the image appending the terms 'red', 'green' and
  'blue' to the orginal filename.
  '''
  name, ext = os.path.splitext(filename)
  img = cv2.imread(filename)
  red, green, blue = split_into_rgb_channels(img)
  for values, color, channel in zip((red, green, blue), ('red', 'green', 'blue'), (2,1,0)):
    img = np.zeros((values.shape[0], values.shape[1], 3), dtype = values.dtype)
    #img[:,:,channel] = values
    # convert to greyscale (i.e. all channels get same value)
    img[:,:,0] = values
    img[:,:,1] = values
    img[:,:,2] = values
    print "Total {}: {}".format(color, sum(sum(values)))
    new_file = name+"_"+color+ext
    print "Saving Image: {}.".format(new_file)
    cv2.imwrite(new_file, img)

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('file',
    help = "File to decompose into RGB channels")
  args = parser.parse_args()
  decompose(args.file)

if __name__ == "__main__":
  main()
