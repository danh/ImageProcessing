#!/usr/bin/env python

"""
Rotate images to be aligned to a master image
"""

def main():
  import argparse
  p = argparse.ArgumentParser()
  p.add_argument('--master', required=True, type=str,
    help = "File to align images to")
  args = p.parse_args()

if __name__ == "__main__":
  main()
