#!/usr/bin/env python
import os
import numpy as np
from PIL import Image
from pylab import hist,show,xlim
import argparse

# Adapted from https://github.com/mariaokounkova/art_online

def do_hist(filename):
  """Histogram the grayscale values"""
  # Read in and convert to grayscale
  im = np.array(Image.open(filename).convert('L'))
  # bins should be a multiple of 2 (since 256 colors)
  hist(im.flatten(), bins=128, normed=1, edgecolor='none', alpha=0.7)
  xlim(0, 256)
  show()

def main():
  p = argparse.ArgumentParser()
  p.add_argument('file',
    help = "Shows the histogram of this file")
  args = p.parse_args()
  do_hist(args.file)

if __name__ == "__main__":
  main()
